<?php
header("Content-Type:application/json");
$json_data='{

    "items": [
        {
            "roll": 1,
            "name": "Scott",
            "surname": "Monroe",
            "fullname": "Jennifer Reid",
            "email": "jerome@schroeder.cu",
            "per": 100
        },
        {
            "roll": 2,
            "name": "Chris",
            "surname": "Odom",
            "fullname": "Clarence Clapp",
            "email": "deborah@roach.sk",
            "per": 100
        },
        {
            "roll": 3,
            "name": "Alan",
            "surname": "Bray",
            "fullname": "Wallace Knowles",
            "email": "sheryl@o.tv",
            "per": 100
        },
        {
            "roll": 4,
            "name": "Troy",
            "surname": "Jenkins",
            "fullname": "Beth Horn",
            "email": "steve@glover.org",
            "per": 100
        },
        {
            "roll": 5,
            "name": "Susan",
            "surname": "Ritchie",
            "fullname": "Sherri Boyd",
            "email": "bob@hughes.ve",
            "per": 100
        },
        {
            "roll": 6,
            "name": "Molly",
            "surname": "Hill",
            "fullname": "Clyde Lucas",
            "email": "claude@dawson.cd",
            "per": 100
        },
        {
            "roll": 7,
            "name": "Veronica",
            "surname": "Li",
            "fullname": "Megan Bowling",
            "email": "hilda@bowers.ky",
            "per": 100
        },
        {
            "roll": 8,
            "name": "Anita",
            "surname": "Gold",
            "fullname": "Ann Garrett",
            "email": "clarence@crabtree.eg",
            "per": 100
        },
        {
            "roll": 9,
            "name": "Penny",
            "surname": "Hicks",
            "fullname": "Carrie Goldman",
            "email": "laurence@schroeder.er",
            "per": 100
        },
        {
            "roll": 10,
            "name": "Rose",
            "surname": "York",
            "fullname": "Sarah Gonzalez",
            "email": "tracy@weber.bo",
            "per": 100
        }
    ]

}';
?>

